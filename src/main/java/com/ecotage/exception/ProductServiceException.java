package com.ecotage.exception;

public class ProductServiceException extends Exception {
	
	public ProductServiceException() {
		super();
	}

	public ProductServiceException(final String message) {
		super(message);
	}

}
